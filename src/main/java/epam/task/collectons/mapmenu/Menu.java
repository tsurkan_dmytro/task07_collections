package epam.task.collectons.mapmenu;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {

    private Map<String,String> menu;
    private Map<String,Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public Menu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - show35");
        menu.put("2", " 2 - show45");
        menu.put("3", " 3 - show65");
        menu.put("4", " 4 - show75");
        menu.put("5", " 5 - show275");
        menu.put("Q", " Q - exit");

        methodsMenu.put("1", this::show35);
        methodsMenu.put("2", this::show45);
        methodsMenu.put("3", this::show65);
        methodsMenu.put("4", this::show75);
        methodsMenu.put("5", this::show275);

    }

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.show();
    }

    private void outputMenu(){
        System.out.println("\n MENU:");

        for (String str: menu.values()){
            System.out.println(str);
        }
    }

    public void show(){
        String keyMenu = null;

        do {
            outputMenu();
            System.out.println("Select menu: ");
            keyMenu = input.nextLine().toUpperCase();
            try{
                methodsMenu.get(keyMenu).print();
            }catch (Exception e){
                System.out.println("menu problem");
            }
        }while (!keyMenu.equals("Q"));
    }

    private void show35(){
        System.out.println("35");
    }
    private void show45(){
        System.out.println("45");
    }
    private void show65(){
        System.out.println("65");
    }
    private void show75(){
        System.out.println("75");
    }
    private void show275(){
        System.out.println("275");
    }

}
