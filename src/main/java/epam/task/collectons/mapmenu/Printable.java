package epam.task.collectons.mapmenu;

@FunctionalInterface
public interface Printable {
    public void print();
}
