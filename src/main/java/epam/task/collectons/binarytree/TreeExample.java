package epam.task.collectons.binarytree;

public class TreeExample {

    public TreeExample() {

        Tree<Integer> tree = new Tree<>(33, null);
        tree.add(5, 35, 1, 20, 4, 17, 31, 99, 18, 19);
        //print tree elements
        tree.print();
        //remove root
        tree.remove(33);
        tree.remove(17);
        tree.print();
        //Check tree elements
        System.out.println(tree);
        System.out.println(tree.left());
        System.out.println(tree.left().left());

        System.out.println(tree.right().left());
    }
}
