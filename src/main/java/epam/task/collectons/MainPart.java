package epam.task.collectons;

import epam.task.collectons.binarytree.TreeExample;
import epam.task.collectons.enummenu.EnumMenu;
import epam.task.collectons.mapmenu.Menu;
import epam.task.collectons.mapmenu.Printable;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainPart {

    private Map<String,String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MainPart() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - Map menu");
        menu.put("2", " 2 - Enum menu");
        menu.put("3", " 3 - Binary Tree");
        menu.put("Q", " Q - exit");

        methodsMenu.put("1", this::showMapMenu);
        methodsMenu.put("2", this::showEnumMenu);
        methodsMenu.put("5", this::showTree);
    }

    public static void main(String[] args) {
        MainPart mainPart = new MainPart();
        mainPart.show();
    }

    private void showMapMenu(){
        Menu menu = new Menu();
        menu.show();
    }
    private void showEnumMenu(){
        EnumMenu enumMenu = new EnumMenu();
        enumMenu.show();
    }
    private void showTree(){
        TreeExample treeExample = new TreeExample();
    }

    public void show(){
        String keyMenu = null;

        do {
            outputMenu();
            System.out.println("Select menu: ");
            keyMenu = input.nextLine().toUpperCase();
            try{
                methodsMenu.get(keyMenu).print();
            }catch (Exception e){
                System.out.println("menu problem");
            }
        }while (!keyMenu.equals("Q"));
    }

    private void outputMenu(){
        System.out.println("\n MENU:");

        for (String str: menu.values()){
            System.out.println(str);
        }
    }
}
