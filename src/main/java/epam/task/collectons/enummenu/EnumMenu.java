package epam.task.collectons.enummenu;

import java.util.Scanner;

public class EnumMenu {
    enum Menu {
        SHOW_AA(1), SHOW_BB(2), SHOW_CC(3), SHOW_DD(4), QUIT(5);

        private int keyBoard;
        Menu(int key) {
            keyBoard = key;
        }
        public int getKey() {
            return keyBoard;
        }
    }

   private static Scanner input = new Scanner(System.in);

   private void outputMenu() {
       System.out.println("\n MENU:");

       for (Menu item : Menu.values()) {
           if(item.getKey()==5){
               {System.out.printf("Quit = %s\n",  item.getKey());}
           } else
               {System.out.printf("Menu = %s\n",  item.getKey());
           }
       }
   }

    public void show(){
        String keyMenu = null;

        do {
            outputMenu();
            System.out.println("Select menu: ");
            keyMenu = input.nextLine().toUpperCase();
            try{
                System.out.println(getMarginValue(Menu.values()[Integer.valueOf(keyMenu)]));
            }catch (Exception e){
            }
        }while (!keyMenu.equals("5"));
    }

    public static String getMarginValue(Menu margin)
    {
        switch (margin.getKey())
        {
            case 1:    return "1 em";
            case 2:    return "12 px";
            case 3:    return "1.5 em";
            case 4:    return "6 px";
            default:   return null;
        }
    }
}
